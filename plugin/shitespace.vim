" Shitespace -- Show bad whitespace
" Updated: 2022-11-08
" Version: 0.3.1
" Source:  https://www.vim.org/scripts/script.php?script_id=5926
" Project: https://gitlab.com/h3xx/vim-shitespace
" Author:  Dan Church [ h3xx{azzat}gmx{dizzot}com; h3xx@Codeberg.org; h3xx@Gitlab ]
" License: GPLv3 (http://www.gnu.org/licenses/gpl.html)
"
" Copyright (C) 2015-2022 Dan Church
" License GPLv3+: GNU GPL version 3 or later (http://gnu.org/licenses/gpl.html).
" This is free software: you are free to change and redistribute it. There is
" NO WARRANTY, to the extent permitted by law.

if !exists('g:shitespaceDefaultOn') || g:shitespaceDefaultOn
	call shitespace#On()
endif
