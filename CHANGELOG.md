# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Add macro support to `g:shitespaceMatch`

## [0.3.1] - 2022-11-08

### Added
- Highlight fancy Unicode whitespace characters

## [0.3.0] - 2022-10-28

### Added
- Add functions for explicitly toggling on/off

### Fixed
- Fix missing match call - if toggling on in the current file, it would fail to highlight

### Removed
- Remove automatic keystroke support

## [0.2.0] - 2022-09-13

### Changed
- Changed installation slightly (vim 7.x); 8.x is unaffected

### Fixed
- Minor improvements

## [0.1.0] - 2021-01-18
Initial published version
