# Vim Shitespace

> **shitespace**
> (**sh&#x12B;tsp&#x101;s**, [IPA](https://en.wiktionary.org/wiki/Appendix:English_pronunciation): **/&#x283;a&#x26A;tspe&#x26A;s/**):<br />
> Unwanted or unintended whitespace, commonly found when
> programming but also in prose.<br />
> *I spent ages tidying up that file for you, next time try leaving out the
> shitespace when you write it*
>
> &mdash; [Urban Dictionary: *shitespace*](https://www.urbandictionary.com/define.php?term=shitespace)

Or, if anyone asks, it's short for "show whitespace." ;-)

Highlights all the bad whitespace in your files:

- Whitespace at the end of lines
- Spaces followed by tabs
- Fancy unicode space characters
- Mixed tabs and spaces (if configured)

![example highlighting](../../raw/images/example.png)

## Installation

Shitespace is on by default. All you have to do to start using it is to put it
in a place where Vim will load it:

Vim 8+:

```sh
mkdir -p ~/.vim/pack/dist/start
git clone https://gitlab.com/h3xx/vim-shitespace.git ~/.vim/pack/dist/start/vim-shitespace
```

Vim 7 and older:

```sh
mkdir -p ~/.vim/{autoload,plugin}
wget -O ~/.vim/autoload/shitespace.vim https://gitlab.com/h3xx/vim-shitespace/-/raw/main/autoload/shitespace.vim
wget -O ~/.vim/plugin/shitespace.vim https://gitlab.com/h3xx/vim-shitespace/-/raw/main/plugin/shitespace.vim
```

## Configuration

Copy and modify these default values to your `~/.vimrc` to tweak how Shitespace
works:

```vim
let g:shitespaceDefaultOn = 1
let g:shitespaceColor = 'red'
" '%nonAsciiSpace%' expands out to '\%u00A0\|\%u2001\|\%u2002\|\%u2004\|\%u2006\|\%u2007\|\%u2008\|\%u2009\|\%u200A\|\%u202F\|\%u205F\|\%u3000'
let g:shitespaceMatch = '/\s\+$\| \+\ze\t\|%nonAsciiSpace%/'
```

## Configuration Cookbook

If you don't want Shitespace on by default, instead activating it with a
keystroke (in this case <kbd>F5</kbd>):

```vim
let g:shitespaceDefaultOn = 0
imap <silent> <F5> <C-o>:call shitespace#Toggle()<cr>
nmap <silent> <F5> :call shitespace#Toggle()<cr>
```

You can assign different keys to toggle shitespace on and off by calling
`shitespace#On()` and `shitespace#Off()` respectively:

```vim
nmap <silent> <F5> :call shitespace#On()<cr>
nmap <silent> <F6> :call shitespace#Off()<cr>
```

## Alternate Regexes

If you don't like mixed spaces-and-tabs, stick this in your ~/.vimrc:

```vim
let g:shitespaceMatch = '/\s\+$\| \+\t\+\|\t\+ \+/'
```

If you don't like tabs used to indent (but they're okay elsewhere in the line):

```vim
let g:shitespaceMatch = '/\s\+$\|^\t\+/'
```

If you don't like spaces used to indent:

```vim
let g:shitespaceMatch = '/\s\+$\|^ \+/'
```

If you don't like tabs AT ALL:

```vim
let g:shitespaceMatch = '/\s\+$\|\t\+/'
```

## Contributing

Send me a pull request! Keep in mind I really want to keep this plugin
lightweight.

## License

Copyright (C) 2021-2022 Dan Church.

License GPLv3+: GNU GPL version 3 or later (http://gnu.org/licenses/gpl.html).
This is free software: you are free to change and redistribute it. There is NO
WARRANTY, to the extent permitted by law.
