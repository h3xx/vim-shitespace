" Shitespace -- Show bad whitespace
" Updated: 2022-11-08
" Version: 0.3.1
" Source:  https://www.vim.org/scripts/script.php?script_id=5926
" Project: https://gitlab.com/h3xx/vim-shitespace
" Author:  Dan Church [ h3xx{azzat}gmx{dizzot}com; h3xx@Codeberg.org; h3xx@Gitlab ]
" License: GPLv3 (http://www.gnu.org/licenses/gpl.html)
"
" Copyright (C) 2015-2022 Dan Church
" License GPLv3+: GNU GPL version 3 or later (http://gnu.org/licenses/gpl.html).
" This is free software: you are free to change and redistribute it. There is
" NO WARRANTY, to the extent permitted by law.

let s:macros = {
	\ '%nonAsciiSpace%': '\%u00A0\|\%u2001\|\%u2002\|\%u2004\|\%u2006\|\%u2007\|\%u2008\|\%u2009\|\%u200A\|\%u202F\|\%u205F\|\%u3000',
	\ }

let s:color = get(g:, 'shitespaceColor', 'red')

" Match
" - Whitespace at the end of lines
" - Spaces followed by tabs
" - Fancy unicode space characters
let s:matchpat = get(g:, 'shitespaceMatch', '/\s\+$\| \+\ze\t\|%nonAsciiSpace%/')

function! s:matchpat_subst(str)
	let sub = a:str
	for [key, value] in items(s:macros)
		let sub = substitute(sub, key, value, 'g')
	endfor
	return sub
endfunction


let s:matchcmd = 'match ExtraWhitespace ' . s:matchpat_subst(s:matchpat)
let s:hlcmd = 'hi ExtraWhitespace term=reverse ctermbg=' . s:color . ' guibg=' . s:color
let s:on = 0

function! shitespace#Off()
	let s:on = 0
	hi clear ExtraWhitespace
	aug Shitespace
		au!
	aug END
endfunction

function! shitespace#On()
	let s:on = 1
	exe s:hlcmd
	exe s:matchcmd
	aug Shitespace
		" If the user changes the colorscheme while Shitespace is on, keep
		" highlights.
		exe 'autocmd ColorScheme * ' . s:hlcmd
		exe 'autocmd BufEnter,WinEnter * ' . s:matchcmd
	aug END
endfunction

function! shitespace#Toggle()
	if s:on
		call shitespace#Off()
	else
		call shitespace#On()
	endif
endfunction
